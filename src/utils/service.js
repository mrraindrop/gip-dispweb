const $axios = require('axios');


const getData = (url = '', params = {}) => {
    return new Promise((resolve, reject) => {
        $axios.get(url, {
                params: params
            })
            .then(function (response) {
                resolve({
                    success: true,
                    data: response.data
                });

            })
            .catch(function (error) {
                reject({
                    success: false,
                    data: error
                });
            });
    })
}

export default {
    getData: getData
}