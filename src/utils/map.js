import service from './service.js';
import $L from "leaflet";
const XmlToJSON = require('xmltojson');
const XmlOptions = {
    mergeCDATA: false,
    xmlns: false,
    attrsAsObject: false
}

const loadWfsGeoJSON = async (url) => {
    let param = {
        request: "GetFeature",
        outputFormat: "JSON",
        typename: "china:border"
    };
    let geoJSON = await service.getData(url, param);
    return geoJSON;
}

const loadWFSLayer = async (map, wfsUrl) => {
    let wfsGeoJson = await loadWfsGeoJSON(wfsUrl);
    let layerRes = null
    if (wfsGeoJson.success) {
        layerRes = $L
            .geoJSON(wfsGeoJson.data, {
                style: function () {
                    return {
                        color: "#3388ff"
                    };
                }
            })
            .bindPopup(function (layer) {
                console.log(layer)
                return "dfasdf ";
                // return " " + layer.feature.properties.tnode_;
            })
            .addTo(map);
    }
    return layerRes;
}

const getWMSLayerNames = async (url, all = false) => {
    let param = {
        'SERVICE': 'WMS',
        'REQUEST': 'GetCapabilities'
    }
    let geoJSON = await service.getData(url, param);
    let wmsCap = XmlToJSON.parseString(geoJSON.data, XmlOptions).WMS_Capabilities[0];
    let layers = wmsCap.Capability[0].Layer[0].Layer;
    let version = wmsCap._attrversion._value

    if (all) {
        let layerName = layers[0].Title[0]._text;
        return {
            version,
            layerName
        }
    } else {
        let layerGroupName = '';
        let layerGroup = [];
        layers.forEach((elem, idx) => {
            if (idx === 0)
                layerGroupName = elem.Title[0]._text
            else
                layerGroup.push(layerGroupName + ':' + elem.Title[0]._text);
        })

        return layerGroup;
    }
}
const loadWMSLayer = async (map, url, all) => {
    let option = {
        layers: await getWMSLayerNames(url, all),
        format: "image/png",
        transparent: true,
        // version: version
    };

    let layerRes = $L.tileLayer.wms(url, option).bindPopup(function (layer) {
        console.log(layer)
        return "dfasdf ";
        // return " " + layer.feature.properties.tnode_;
    }).addTo(map);
    return layerRes;
}

const loadTileLayer = async (map, url) => {
    let layerRes = $L.tileLayer(url).addTo(map);
    return layerRes;
}

const createLayerGroup = async (map, layergroup) => {
    let group = $L.layerGroup(layergroup);
    return group;
}

const zoomInMap = (map) => {
    map.zoomIn();
}
const zoomOutMap = (map) => {
    map.zoomOut();
}

export default {
    loadWFSLayer: loadWFSLayer,
    getWMSLayerNames: getWMSLayerNames,
    loadWMSLayer: loadWMSLayer,
    loadWfsGeoJSON: loadWfsGeoJSON,
    loadTileLayer: loadTileLayer,
    zoomInMap: zoomInMap,
    zoomOutMap: zoomOutMap,
    createLayerGroup: createLayerGroup
}