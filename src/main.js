import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index.js'
import utils from './utils'
import url from './api/url.js'
import api from './api/api.js'


import './assets/style/index.less'
import 'leaflet/dist/leaflet.css'
// 引入iview
import 'iview/dist/styles/iview.css'
import iview from "@/components/custom/iview"
Vue.use(iview)
// 引入elementUI
import 'element-ui/lib/theme-chalk/index.css';
import ElementUI from '@/components/custom/elementUI'
Vue.use(ElementUI);
// 映入第三方或自定义组件
import custom from '@/components/custom'
Vue.use(custom);
//配置自定义样式
import './assets/css/admin/style.css'

Vue.config.productionTip = false
Vue.prototype.$utils = utils
Vue.prototype.$url = url
Vue.prototype.$api = api
Vue.prototype.$store = store

// 引入api接口地址配置
const apiConfig = require('./api/config.js')
Vue.prototype.$baseUrl = apiConfig.baseUrl
Vue.prototype.$sourceUrl = apiConfig.sourceUrl

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')