import Vue from 'vue'
import Router from 'vue-router'
import home from '@/views/admin/home'
import mapComponentsConfig from '@/views/admin/map-components-config'
import graphAnalysisConfig from '@/views/admin/graph-analysis-config'
import primitiveAnalysisConfig from '@/views/admin/primitive-analysis-config'

import store from '@/store/index.js'

// 展示
import mapHome from '@/views/display/Home.vue'


Vue.use(Router)
const router = new Router({
    routes: [{
        path: '/',
        redirect: '/admin/map-components-config'
    }, {
        path: '/map',
        component: mapHome
    }, {
        path: '/admin',
        component: home,
        children: [{
            path: 'map-components-config',
            component: mapComponentsConfig,
            meta: {
                title: '地图组件配置',
                pageName: 'map-components-config'
            }
        }, {
            path: 'graph-analysis-config',
            component: graphAnalysisConfig,
            meta: {
                title: '图形分析配置',
                pageName: 'graph-analysis-config'
            }
        }, {
            path: 'primitive-analysis-config',
            component: primitiveAnalysisConfig,
            meta: {
                title: '图元分析配置',
                pageName: 'primitive-analysis-config'
            }
        }, ]
    }]
})

router.beforeEach((to, from, next) => {
    const meta = to.meta;
    document.title = meta.title || '图形展示分析配置';
    if (meta && meta.title) store.commit('sidebarnav/changeActive', meta.pageName)
    next()
});

export default router