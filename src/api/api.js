import axios from './axios.js'
import url from './url.js'
export default {
  getToolBarConfig() {
    return axios.get(url.get.getToolBarConfig)
  },
  getBaseTools() {
    return axios.get(url.get.getBaseTools)
  },
  getComponents() {
    return axios.get(url.get.getComponents)
  },
  sortToolbar(data) {
    return axios.post(url.post.sortToolbar, data)
  },
  addTool(data) {
    return axios.post_prompt(url.post.addTool, data)
  },
  saveTool(data) {
    return axios.post_prompt(url.post.saveTool, data)
  },
  removeTool(data) {
    return axios.post_prompt(url.post.removeTool, data)
  },
  addGroup(data) {
    return axios.post_prompt(url.post.addGroup, data)
  },
  removeGroup(data) {
    return axios.post_prompt(url.post.removeGroup, data)
  },
  saveComponentsConfig(data) {
    return axios.post_prompt(url.post.saveComponentsConfig, data)
  },
  getConfServer() {
    return axios.get(url.get.getConfServer)
  },
  getConfPixel(id) {
    return axios.get(url.get.getLayer + id)
  },
  getChartList(url) {
    return axios.get(url)
  },
  getLayer(serverId) {
    return axios.get(url.get.getLayer + serverId)
  },
  getLayerConfig(serverId) {
    return axios.get(url.get.getLayerConfig + serverId)
  },
  addChart(url, data) {
    return axios.post_prompt(url, data)
  },
  updateChart(url, data) {
    return axios.post_prompt(url, data)
  },
  deleteChart(url, data) {
    return axios.post_prompt(url, data)
  },
  saveLayer(data) {
    return axios.post_prompt(url.post.saveLayer, data)
  },
  addLayer(data) {
    return axios.post_prompt(url.post.addLayer, data)
  }
}