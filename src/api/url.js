export default {
  post: {
    sortToolbar: "/as/config/tool/order",
    addTool: '/as/config/tool/add',
    saveTool: '/as/config/tool/update',
    removeTool: "/as/config/tool/delete",
    addGroup: "/as/config/tool/group/add",
    removeGroup: "/as/config/tool/group/delete",
    saveComponentsConfig: "/as/config/comps/update",
    addChart: "/as/config/server/chart/add", //添加图层分析图表
    updateChart: "/as/config/server/chart/update", //更新图层分析图表
    deleteChart: "/as/config/server/chart/delete", //删除图层分析图表

    addPixel: "/as/config/server/pixel/chart/add", //添加图层分析图表
    updatePixel: "/as/config/server/pixel/chart/update", //更新图层分析图表
    deletePixel: "/as/config/server/pixel/chart/delete", //删除图层分析图表

    saveLayer: "/as/config/server/display/update", // 保存图层展示
    addLayer: "/as/config/server/display/add" // 新增图层展示


  },
  get: {
    getToolBarConfig: '/as/config/toolbar',
    getBaseTools: '/as/config/tool/orig',
    getComponents: '/as/config/comp',
    getConfServer: '/as/config/server', // 获取图形服务的下拉列表
    getChartList: '/as/config/server/chart/', // 获取图层分析图表列表
    getLayer: "/as/config/server/",// 获取图形服务中的图层列表
    getLayerConfig: "/as/config/server/display/",
    getPixelList: '/as/config/server/pixel/chart/', // 获取图层分析图表列表
  }
}