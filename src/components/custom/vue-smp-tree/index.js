import vueSimpleTree from './vue-smp-tree.vue'

const SmpTree = {
    install: Vue => {
        Vue.component('SmpTree', vueSimpleTree)
    }
}
export default SmpTree