// 资源服务目录树
import SmpTree from './vue-smp-tree';
// 右键菜单
import VueContextMenu from 'vue-contextmenu'

const custom = {
    install: Vue => {
        Vue.use(SmpTree);
        Vue.use(VueContextMenu);
    }
}

export default custom