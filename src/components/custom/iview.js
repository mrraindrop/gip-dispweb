import {
  Button,
  Select,
  Option,
  Input,
  Checkbox,
  Poptip,
  Page,
  Modal,
  Upload,
  Icon,
  Carousel,
  CarouselItem,
  Col,
  Spin,
  Table,
  Progress,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  ButtonGroup,
  Steps,
  Step,
  RadioGroup,
  Radio,
  FormItem,
  Form,
  Tag,
  InputNumber,
  Message,
  Row,
  Tabs,
  TabPane
} from 'iview';

const iview = {
  install: function (Vue) {
    Vue.component('Button', Button);
    Vue.component('Select', Select);
    Vue.component('Option', Option);
    Vue.component('Input', Input);
    Vue.component('Checkbox', Checkbox);
    Vue.component('Poptip', Poptip);
    Vue.component('Page', Page);
    Vue.component('Modal', Modal);
    Vue.component('Upload', Upload);
    Vue.component('Icon', Icon);
    Vue.component('Carousel', Carousel);
    Vue.component('CarouselItem', CarouselItem);
    Vue.component('Col', Col);
    Vue.component('Row', Row);

    Vue.component('Spin', Spin);
    Vue.component('Table', Table);
    Vue.component('Progress', Progress);

    Vue.component('Dropdown', Dropdown);
    Vue.component('DropdownMenu', DropdownMenu);
    Vue.component('DropdownItem', DropdownItem);
    Vue.component('ButtonGroup', ButtonGroup);
    Vue.component('Steps', Steps);
    Vue.component('Step', Step);
    Vue.component('RadioGroup', RadioGroup);
    Vue.component('Radio', Radio);
    Vue.component('FormItem', FormItem);
    Vue.component('Form', Form);
    Vue.component('Tag', Tag);
    Vue.component('InputNumber', InputNumber);
    Vue.component('Tabs', Tabs);
    Vue.component('TabPane', TabPane);

    Vue.prototype.$Message = Message
    Vue.prototype.$Modal = Modal
  }
}

export default iview
