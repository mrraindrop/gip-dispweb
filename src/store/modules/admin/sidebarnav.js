const state = {
    show: true,
    activeSidebar: 'map-components-config'
}

const mutations = {
    display(state, status) {
        if (status) {
            state.show = true
        } else {
            state.show = false
        }
    },
    changeActive(state, val) {
        state.activeSidebar = val
    }
}

const getters = {
    show: state => state.show,
    activeSidebar: state => state.activeSidebar
}

const actions = {
    display({
        commit
    }, status) {
        commit('display', status)
    },
    changeActive({
        commit
    }, val) {
        commit('changeActive', val)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}