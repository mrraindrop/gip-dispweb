import Vue from 'vue'
import Vuex from 'vuex'
import sidebarnav from './modules/admin/sidebarnav'
Vue.use(Vuex)

// const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        sidebarnav
    }
})