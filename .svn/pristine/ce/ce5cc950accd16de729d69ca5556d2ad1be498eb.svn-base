<template>
  <div class="g-simple-tree">
    <div :class="['g-simple-tree g-st__wrap',{opened: idxes.length > 0}]">
      <h4 v-if="title" v-text="title" @click="switchLayers"></h4>
      <ul>
        <draggable v-model="listData" :options="{group:'layers', sort:true, animation:600, handle:'.bg-move'}" @start="hiddenAll">
          <transition-group>
            <li :class="[{active: idxes.indexOf(idx)>-1}]" v-for="(item, idx) in listData" :key="idx" @click="itemClick($event, idx)">
              <div class="g-st__header">
                <div class="g-st__caption">
                  <!-- 图层显隐 checkBox -->
                  <span :class="[{active:item.isShow === true}]" @click.stop="loadLayer(item)" v-html="formatCheckBox(item.isShow)">
                    <!-- 图层名称 -->
                  </span>{{item.name}}</div>
                <div class="g-st__context-memu">
                  <!-- 工具栏 -->
                  <div class="u-tool bg-locate" @click.stop="locate" title="定位图层"></div>
                  <div class="u-tool bg-attr" @click.stop="openAttr" title="属性表"></div>
                  <div class="u-tool bg-analysis" @click.stop="analysis" title="图层分析"></div>
                  <div class="u-tool bg-remove" @click.stop="remove" title="移除图层"></div>
                  <div class="u-tool bg-move" @click.stop="hiddenAll" title="移动"></div>
                </div>
              </div>
              <transition enter-active-class="animated fadeIn" leave-active-class="animated faster fadeOut">
                <!-- 图层例等 -->
                <div class="g-st__body" v-show="idxes.indexOf(idx)>-1" @click.stop="">
                  <img :src="item.legend" alt="">
                </div>
              </transition>
            </li>
          </transition-group>
        </draggable>
      </ul>
    </div>
  </div>
</template>

<script>
import "animate.css";
import Draggable from "vuedraggable";
import Sortable from "sortablejs";

export default {
  name: "vue-smp-tree",
  components: {
    Draggable,
    Sortable
  },
  props: {
    dataset: {
      type: Array,
      require: true
    },
    title: {
      type: String
    },
    locate: {
      type: Function,
      require: true
    },
    openAttr: {
      type: Function,
      require: true
    },
    analysis: {
      type: Function,
      require: true
    },
    remove: {
      type: Function,
      require: true
    },
    resort: {
      type: Function,
      require: true
    },
    dataUpdate: {
      type: Function,
      require: true
    },
    ctrlLayer: {
      type: Function,
      require: true
    }
  },
  computed: {
    listData: {
      get: function() {
        return this.dataset;
      },
      set: function(newValue) {
        this.dataUpdate(newValue);
      }
    }
  },
  data() {
    return {
      idxes: [],
      list: [
        {
          name: "中国地图1",
          legend: "url",
          order: 1
        },
        {
          name: "中国地图adfa",
          legend: "url",
          order: 2
        },
        {
          name: "中国地图5435dgs",
          legend: "url",
          order: 3
        },
        {
          name: "中国地图65754gfdsg",
          legend: "url",
          order: 4
        }
      ]
    };
  },
  filter: {},
  methods: {
    itemClick(evt, idx) {
      let isExist = this.idxes.indexOf(idx);
      isExist > -1 ? this.idxes.splice(isExist, 1) : this.idxes.push(idx);

      if (isExist === -1) {
        let prevEl = evt.currentTarget.previousElementSibling,
          nextEl = evt.currentTarget.nextElementSibling;

        if (prevEl !== null)
          prevEl.className.indexOf("active-prev") === -1
            ? (prevEl.className += " active-prev")
            : " ";

        if (nextEl !== null)
          nextEl.className.indexOf("active-next") === -1
            ? (nextEl.className += " active-next")
            : " ";
      }
    },
    loadLayer(layer) {
      if (layer.isShow !== undefined) {
        layer.isShow = !layer.isShow;
      } else {
        this.$set(layer, "isShow", true);
      }
      this.ctrlLayer(layer);
    },
    formatCheckBox: function(val) {
      return val === true ? "&#x2714;" : "";
    },
    hiddenAll() {
      let list = document
        .getElementsByClassName("g-simple-tree")[0]
        .querySelectorAll("li");
      list.forEach(element => {
        element.className = "";
      });
      this.idxes = [];
    },
    openAll() {
      this.idxes = [];
      let list = document
        .getElementsByClassName("g-simple-tree")[0]
        .querySelectorAll("li");
      list.forEach((element, index) => {
        element.className = "active";
        this.idxes.push(index);
      });
    },
    switchLayers() {
      this.idxes.length > 0 ? this.hiddenAll() : this.openAll();
    }
  }
};
</script>

<style lang="less" scoped>
.g-simple-tree {
  position: relative;
  width: 100%;
  padding-left: 15px;
  overflow: auto;
  overflow-y: auto;
  margin: 15px 0;

  h4 {
    padding: 0 15px;
    font-size: 12px;
    font-weight: 400;
    vertical-align: text-top;
    cursor: pointer;
  }

  ul {
    margin-top: 10px;
    padding-left: 30px;
    width: 100%;
    overflow: auto;

    li {
      padding: 0;
      padding-right: 15px;
      line-height: 32px;
      transition: all 0.6;

      .g-st__header {
        position: relative;

        // 标题、选中、图层类型
        .g-st__caption {
          display: inline;
          margin-right: 15px;
          cursor: pointer;
          -webkit-user-select: none;
          color: #909090;

          span {
            display: inline-block;
            margin: 0 2px;
            border-radius: 2px;
            width: 12px;
            height: 12px;
            line-height: 12px;
            text-align: center;
            background-color: #fff;
            border: 1px solid #ddd;
            vertical-align: text-top;
          }

          span.active {
            color: #fff;
            background-color: #1e6add;
            border: 0;
          }

          span:hover {
            box-shadow: 0 0 2px rgba(30, 106, 221, 1);
          }
        }

        .g-st__context-memu {
          float: right;
          display: none;
          transition: all 0.6s;

          .u-tool {
            display: inline-block;
            margin: 0 5px;
            width: 16px;
            height: 16px;
            vertical-align: text-bottom;
          }

          .u-tool:hover {
            background-color: #ddd;
          }

          .u-tool.bg-handle {
            cursor: move;
          }
        }
      }

      // 节点样式
      .g-st__header::before {
        position: absolute;
        top: 0;
        left: 0;
        margin-left: -15px;
        width: 12px;
        height: 32px;
        line-height: 32px;
        overflow: hidden;
        cursor: pointer;
        content: url("./img/st-node+md.png");
        text-align: center;
      }

      .g-st__body {
        position: relative;
        background-color: #fff;
        // padding: 30px;
        margin-left: -15px;

        img {
          max-width: 100%;
        }
      }
    }

    li:first-child {
      .g-st__header::before {
        content: url("./img/st-node+next.png");
      }
    }

    li:last-child {
      .g-st__header::before {
        content: url("./img/st-node+prev.png");
      }
    }

    li:first-child.active-prev {
      .g-st__header::before {
        content: url("./img/st-node+next.png");
      }
    }

    li:last-child.active-next {
      .g-st__header::before {
        content: url("./img/st-node+prev.png");
      }
    }

    // 鼠标移动至列表项显示功能菜单
    li:hover {
      .g-st__context-memu {
        display: inline;
      }
    }

    li.active {
      .g-st__header {
        .g-st__caption {
          color: #303030;
        }
      }

      // 节点样式
      .g-st__header::before {
        content: url("./img/st-node-active.png") !important;
      }
    }

    li.active-prev.active-next {
      // 节点样式
      .g-st__header::before {
        content: url("./img/st-node+md.png");
      }
    }
  }
}

.bg-remove {
  width: 16px;
  height: 16px;
  background: url("./img/tree-tools.png") -10px -10px;
}

.bg-analysis {
  width: 16px;
  height: 16px;
  background: url("./img/tree-tools.png") -46px -10px;
}

.bg-move {
  width: 16px;
  height: 16px;
  background: url("./img/tree-tools.png") -10px -46px;
}

.bg-locate {
  width: 16px;
  height: 16px;
  background: url("./img/tree-tools.png") -46px -46px;
}

.bg-attr {
  width: 16px;
  height: 16px;
  background: url("./img/tree-tools.png") -82px -10px;
}

.g-st__wrap {
  overflow: auto;

  ul {
    max-height: 600px;
  }
}

.g-st__wrap::before {
  position: absolute;
  left: 10px;
  width: 12px;
  height: 100%;
  line-height: 32px;
  overflow: hidden;
  cursor: pointer;
  content: url("./img/st-node+start.png");
  text-align: center;
  background-image: url("./img/st-line.png");
  background-repeat: repeat-y;
  background-position: center 200px;
  background-origin: content-box;
}

.g-st__wrap.opened::before {
  content: url("./img/st-node-end.png");
}
</style>